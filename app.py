from flask import Flask
from flask import request, Response
import serial
from flask_cors import CORS
import modulovisao
import modulomqtt
import threading

app = Flask(__name__)
CORS(app)

t = threading.Thread(target=modulovisao.run)
t.start()

def img():
    while True:
        yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + modulovisao.getCurrentFrame() + b'\r\n')

@app.route('/video.mjpg')
def video():
    return Response(img(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')



if __name__ == '__main__':
    app.run(host='0.0.0.0')
