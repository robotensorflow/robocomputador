import time
import smbus2
import sys

slaveAddress = 0x18

i2c = smbus2.SMBus(1)


def run():
    i2c.write_i2c_block_data(slaveAddress, 0,  sys.argv[1].encode())

def enviar(data):
    i2c.write_i2c_block_data(slaveAddress, 0, data.encode())


if __name__ == '__main__':
    run()
