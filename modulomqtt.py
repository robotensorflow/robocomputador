import paho.mqtt.client as mqtt
import serial
import moduloi2c
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("robo")
	
def on_message(client, userdata, msg):
    command = msg.payload.decode("utf-8") + '\r\n'
    print("enviando " + str(msg.payload.decode("utf-8")))
    moduloi2c.enviar(command)
    print(msg.topic+" "+str(msg.payload.decode("utf-8")))


def run():
    print("Cliente MQTT iniciado!")
    client = mqtt.Client(transport="websockets")
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect("127.0.0.1", 9001, 60)
    print("Conectando...")
    client.loop_forever()


if __name__ == '__main__':
    run()
