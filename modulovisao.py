from flask import Flask
from flask_cors import CORS
import processadorTF
import threading
from pubsub import pub
import cv2
import numpy as np
from utils import visualization_utils as vis_util
from picamera.array import PiRGBArray
from picamera import PiCamera

app = Flask(__name__)
CORS(app)

t = threading.Thread(target=processadorTF.run)
t.start()

global boxes
global classes
global scores
global category_index
boxes = None
classes = None
scores = None
category_index = None

def listener1(arg1,arg2,arg3,arg4, arg5):
    global boxes
    global classes
    global scores
    global category_index
    if(arg4 is not None):
        for index in range(0, len(arg3)):
            if (arg3[index] >  0.4):
                category_index = arg4
                boxes = arg1
                classes = arg2
                scores = arg3

pub.subscribe(listener1, 'rootTopic')

imageResult = np.array(cv2.imencode('.jpg',cv2.imread("noimage.png"))).tostring();
def getCurrentFrame():
    global imageResult
    return imageResult

def run():
    camera = PiCamera()
    camera.resolution = (640, 480)
    camera.framerate = 32
    rawCapture = PiRGBArray(camera, size=(640, 480))
    print("iniciando")
    ret = True
    while (ret):
        global boxes
        global classes
        global scores
        global category_index
        camera.capture(rawCapture, format="bgr")
        image_np = np.array(rawCapture.array, copy=True)
        pub.sendMessage('capture', image_np=image_np)

        if(boxes is not None):
            vis_util.visualize_boxes_and_labels_on_image_array(
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                line_thickness=8)

        imresized = cv2.resize(image_np, (640, 480))
        ret, jpeg = cv2.imencode('.jpg', imresized)
        global imageResult
        imageResult = np.array(jpeg).tostring()
        rawCapture.truncate(0)


if __name__ == '__main__':

    app.run(host='0.0.0.0')
